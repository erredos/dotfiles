#!/usr/bin/env bash

function run_one() {
  p=$1
  path="${HOME}/.cache/${p}.run_one"
  flock -w 1 "${path}" "$@"
}

exec xss-lock slock
